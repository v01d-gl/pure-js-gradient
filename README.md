# Simple gradient written in JS


## How to use

* Include file `gradient.js`
* Create Gradient instanse

```JS
new Gradient();
new Gradient(config_object);
new Gradient(element, config_object);

/*
element types:
    HTMLElement
    String (CSS selector)
*/
```

* Now you are able to start/stop it with

```JS
grad.start();
grad.stop();
```

## Note
Use class `BorderGradient` if you need one


## Configuration
```JS
{
    grad_coeff: [1, 3, 2],
    max: [255, 255, 255],
    min: [0, 0, 0],
    init_color: [50, 27, 10],
    delay: 10,
    scene: 'background',
}
```
